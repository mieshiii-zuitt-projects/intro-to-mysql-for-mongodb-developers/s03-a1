Add users:

INSERT INTO users (email, password, datetime_created)
VALUES ("johnsmith@gmail.com", "passwordA", 20210101010000),
("juandelacruz@gmail.com", "passwordB", 20210101020000),
("janesmith@gmail.com", "passwordC", 20210101030000),
("mariadelacruz@gmail.com", "passwordD", 20210101040000),
("johndoe@gmail.com", "passwordE", 20210101050000);

Add posts:

INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES (1, "First Code", "Hello World!", 20210101010000),
(1, "Second Code", "Hello Earth!", 20210101020000),
(2, "Third Code", "Welcome to Mars!", 20210101030000),
(4, "Fourth Code", "Bye bye Solar System!", 20210101040000);

Q1: 
SELECT *
FROM posts
WHERE author_id = 1;

Q2:
SELECT email, datetime_created FROM users;

Q3:
UPDATE posts
SET content = "Hello to the people of the Earth!"
WHERE id = 2;

Q4:
DELETE FROM users
WHERE email = "johndoe@gmail.com";
